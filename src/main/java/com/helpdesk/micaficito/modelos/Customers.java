package com.helpdesk.micaficito.modelos;
// Generated 11-dic-2019 11:52:12 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Customers generated by hbm2java
 */
@Entity
@Table(name="customers"
    ,catalog="openbravopos"
    , uniqueConstraints = @UniqueConstraint(columnNames="SEARCHKEY") 
)
public class Customers  implements java.io.Serializable {


     private String id;
     private Taxcustcategories taxcustcategories;
     private String searchkey;
     private String taxid;
     private String name;
     private String card;
     private double maxdebt;
     private String address;
     private String address2;
     private String postal;
     private String city;
     private String region;
     private String country;
     private String firstname;
     private String lastname;
     private String email;
     private String phone;
     private String phone2;
     private String fax;
     private String notes;
     private boolean visible;
     private Date curdate;
     private Double curdebt;
     private Set<ReservationCustomers> reservationCustomerses = new HashSet<ReservationCustomers>(0);
     private Set<Tickets> ticketses = new HashSet<Tickets>(0);

    public Customers() {
    }

	
    public Customers(String id, String searchkey, String name, double maxdebt, boolean visible) {
        this.id = id;
        this.searchkey = searchkey;
        this.name = name;
        this.maxdebt = maxdebt;
        this.visible = visible;
    }
    public Customers(String id, Taxcustcategories taxcustcategories, String searchkey, String taxid, String name, String card, double maxdebt, String address, String address2, String postal, String city, String region, String country, String firstname, String lastname, String email, String phone, String phone2, String fax, String notes, boolean visible, Date curdate, Double curdebt, Set<ReservationCustomers> reservationCustomerses, Set<Tickets> ticketses) {
       this.id = id;
       this.taxcustcategories = taxcustcategories;
       this.searchkey = searchkey;
       this.taxid = taxid;
       this.name = name;
       this.card = card;
       this.maxdebt = maxdebt;
       this.address = address;
       this.address2 = address2;
       this.postal = postal;
       this.city = city;
       this.region = region;
       this.country = country;
       this.firstname = firstname;
       this.lastname = lastname;
       this.email = email;
       this.phone = phone;
       this.phone2 = phone2;
       this.fax = fax;
       this.notes = notes;
       this.visible = visible;
       this.curdate = curdate;
       this.curdebt = curdebt;
       this.reservationCustomerses = reservationCustomerses;
       this.ticketses = ticketses;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false)
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TAXCATEGORY")
    public Taxcustcategories getTaxcustcategories() {
        return this.taxcustcategories;
    }
    
    public void setTaxcustcategories(Taxcustcategories taxcustcategories) {
        this.taxcustcategories = taxcustcategories;
    }

    
    @Column(name="SEARCHKEY", unique=true, nullable=false)
    public String getSearchkey() {
        return this.searchkey;
    }
    
    public void setSearchkey(String searchkey) {
        this.searchkey = searchkey;
    }

    
    @Column(name="TAXID")
    public String getTaxid() {
        return this.taxid;
    }
    
    public void setTaxid(String taxid) {
        this.taxid = taxid;
    }

    
    @Column(name="NAME", nullable=false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="CARD")
    public String getCard() {
        return this.card;
    }
    
    public void setCard(String card) {
        this.card = card;
    }

    
    @Column(name="MAXDEBT", nullable=false, precision=22, scale=0)
    public double getMaxdebt() {
        return this.maxdebt;
    }
    
    public void setMaxdebt(double maxdebt) {
        this.maxdebt = maxdebt;
    }

    
    @Column(name="ADDRESS")
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

    
    @Column(name="ADDRESS2")
    public String getAddress2() {
        return this.address2;
    }
    
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    
    @Column(name="POSTAL")
    public String getPostal() {
        return this.postal;
    }
    
    public void setPostal(String postal) {
        this.postal = postal;
    }

    
    @Column(name="CITY")
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }

    
    @Column(name="REGION")
    public String getRegion() {
        return this.region;
    }
    
    public void setRegion(String region) {
        this.region = region;
    }

    
    @Column(name="COUNTRY")
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }

    
    @Column(name="FIRSTNAME")
    public String getFirstname() {
        return this.firstname;
    }
    
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    
    @Column(name="LASTNAME")
    public String getLastname() {
        return this.lastname;
    }
    
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    
    @Column(name="EMAIL")
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    
    @Column(name="PHONE")
    public String getPhone() {
        return this.phone;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }

    
    @Column(name="PHONE2")
    public String getPhone2() {
        return this.phone2;
    }
    
    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    
    @Column(name="FAX")
    public String getFax() {
        return this.fax;
    }
    
    public void setFax(String fax) {
        this.fax = fax;
    }

    
    @Column(name="NOTES")
    public String getNotes() {
        return this.notes;
    }
    
    public void setNotes(String notes) {
        this.notes = notes;
    }

    
    @Column(name="VISIBLE", nullable=false)
    public boolean isVisible() {
        return this.visible;
    }
    
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CURDATE", length=19)
    public Date getCurdate() {
        return this.curdate;
    }
    
    public void setCurdate(Date curdate) {
        this.curdate = curdate;
    }

    
    @Column(name="CURDEBT", precision=22, scale=0)
    public Double getCurdebt() {
        return this.curdebt;
    }
    
    public void setCurdebt(Double curdebt) {
        this.curdebt = curdebt;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="customers")
    public Set<ReservationCustomers> getReservationCustomerses() {
        return this.reservationCustomerses;
    }
    
    public void setReservationCustomerses(Set<ReservationCustomers> reservationCustomerses) {
        this.reservationCustomerses = reservationCustomerses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="customers")
    public Set<Tickets> getTicketses() {
        return this.ticketses;
    }
    
    public void setTicketses(Set<Tickets> ticketses) {
        this.ticketses = ticketses;
    }




}


