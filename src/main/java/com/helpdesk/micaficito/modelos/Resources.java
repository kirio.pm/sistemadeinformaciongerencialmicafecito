package com.helpdesk.micaficito.modelos;
// Generated 11-dic-2019 11:52:12 by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Resources generated by hbm2java
 */
@Entity
@Table(name="resources"
    ,catalog="openbravopos"
    , uniqueConstraints = @UniqueConstraint(columnNames="NAME") 
)
public class Resources  implements java.io.Serializable {


     private String id;
     private String name;
     private int restype;
     private byte[] content;

    public Resources() {
    }

	
    public Resources(String id, String name, int restype) {
        this.id = id;
        this.name = name;
        this.restype = restype;
    }
    public Resources(String id, String name, int restype, byte[] content) {
       this.id = id;
       this.name = name;
       this.restype = restype;
       this.content = content;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false)
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

    
    @Column(name="NAME", unique=true, nullable=false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="RESTYPE", nullable=false)
    public int getRestype() {
        return this.restype;
    }
    
    public void setRestype(int restype) {
        this.restype = restype;
    }

    
    @Column(name="CONTENT")
    public byte[] getContent() {
        return this.content;
    }
    
    public void setContent(byte[] content) {
        this.content = content;
    }




}


