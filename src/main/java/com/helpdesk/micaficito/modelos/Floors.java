package com.helpdesk.micaficito.modelos;
// Generated 11-dic-2019 11:52:12 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Floors generated by hbm2java
 */
@Entity
@Table(name="floors"
    ,catalog="openbravopos"
    , uniqueConstraints = @UniqueConstraint(columnNames="NAME") 
)
public class Floors  implements java.io.Serializable {


     private String id;
     private String name;
     private byte[] image;
     private Set<Places> placeses = new HashSet<Places>(0);

    public Floors() {
    }

	
    public Floors(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public Floors(String id, String name, byte[] image, Set<Places> placeses) {
       this.id = id;
       this.name = name;
       this.image = image;
       this.placeses = placeses;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false)
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

    
    @Column(name="NAME", unique=true, nullable=false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="IMAGE")
    public byte[] getImage() {
        return this.image;
    }
    
    public void setImage(byte[] image) {
        this.image = image;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="floors")
    public Set<Places> getPlaceses() {
        return this.placeses;
    }
    
    public void setPlaceses(Set<Places> placeses) {
        this.placeses = placeses;
    }




}


