/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.micaficito.servicios;

import com.helpdesk.micaficito.dao.EstadoDao;
import com.helpdesk.micaficito.modelos.Estado;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author programacion
 */
@Stateless
public class EstadoServiceImpl implements EstadoService{
    @EJB
    private EstadoDao dao;

    @Override
    public List<Estado> listaEstado() {
       return dao.listaEstado();
    }

    @Override
    public void nuevoEstado(Estado estado) {
        dao.nuevoEstado(estado);
    }

    @Override
    public void actualizarEstado(Estado estado) {
        dao.actualizarEstado(estado);
    }

    @Override
    public Estado buscarEstado(Long idEstado) {
        return dao.buscarEstado(idEstado);
    }

    @Override
    public void eleminarEstado(Estado estado) {
        dao.eleminarEstado(estado);
    }
    
}
