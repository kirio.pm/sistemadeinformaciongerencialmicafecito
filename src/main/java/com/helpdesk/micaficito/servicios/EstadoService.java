/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.micaficito.servicios;

import com.helpdesk.micaficito.modelos.Estado;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author programacion
 */
@Local
public interface EstadoService {
    public List<Estado> listaEstado();
    public void nuevoEstado(Estado estado);
    public void actualizarEstado(Estado estado);
    public Estado buscarEstado(Long idEstado);
    public void eleminarEstado(Estado estado);
}
